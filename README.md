# The Origin
## Description
This mod adds a single indestructible block, called “The Origin”, once at
position (0,-1,0) when the singlenode map generator is used, so players spawn
on a solid block instead of falling down immediately. From this block onwards,
players can build stuff to it just like with any other block.

If `static_spawnpoint` is set, the Origin will be set one node length below
that point instead.

## Current version
The current version is 1.3.1.

## Technical notes
The mod tries to ensure that the Origin will only be set once per world.
If the Origin has been set, a file called “origin.mt” will be placed in the
world's folder. It contains the position of the Origin. As long as this file
is present, this mod will not place the Origin again.

## License
License of everything in this mod: WTFPL
