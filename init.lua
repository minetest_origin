--[[
The Origin
version 1.3.0
]]

local S
if (minetest.get_modpath("intllib")) then
	S = intllib.Getter()
else
	S = function ( s ) return s end
end

local origin = {}
origin.settings = {}

do
	local filepath = minetest.get_worldpath().."/origin.mt"
	local file = io.open(filepath, "r")
	if file then
		io.close(file)
		origin.exists = true
	else
		origin.exists = false
	end

	origin.settings.force_singlenode = minetest.settings:get_bool("origin_force_singlenode")
	if origin.settings.force_singlenode == nil then
		origin.settings.force_singlenode = true
	end
end

minetest.register_node("origin:origin",{
	description = S("The Origin"),
	_doc_items_longdesc = S("The Origin is the starting block of this world. On the Origin the earliest visitors of this world arrive and it is the first block at which all other blocks can be built to, so that builders can build their architectural masterpieces in a world which would be (usually) empty otherwise."),
	groups = { not_in_creative_inventory = 1, immortal = 1 },
	diggable = false,
	tiles = {"origin_origin.png"},
	sounds = { footstep = "origin_origin_footstep" },
	is_ground_content = false,
	drop = "",
})

minetest.register_on_mapgen_init(function(mgparams)
	if origin.settings.force_singlenode == true then
		minetest.set_mapgen_params({mgname = "singlenode"})
	end
end)

minetest.register_on_generated(function(minp, maxp, seed)
	local spawn = minetest.setting_get_pos("static_spawnpoint")
	if origin.exists ~= true then
		local blockpos
		if spawn ~= nil then
			blockpos = { x=spawn.x, y=spawn.y-1, z=spawn.z }
		else
			blockpos = { x=0, y=-1, z=0 }
		end
		if(minp.x <= blockpos.x and maxp.x >= blockpos.x and minp.y <= blockpos.y and maxp.y >= blockpos.y and minp.z <= blockpos.z and maxp.z >= blockpos.z) then
			minetest.set_node(blockpos, {name = "origin:origin"})
			minetest.log("action", "[origin] The Origin has been set at "..minetest.pos_to_string(blockpos)..".")
			origin.exists = true
			local filepath = minetest.get_worldpath().."/origin.mt"
			local file = io.open(filepath, "w")
			if file then
				file:write(minetest.pos_to_string(blockpos))
			else
				minetest.log("error", "[origin] Failed to write origin data into "..filepath..". The Origin may be placed again if you change static_spawnpoint.")
			end
		end
	end
end)
